# Haskell-Rust FFI Experiments

This repository contains some examples of calling Rust functions from Haskell.

## The Examples

For now, you will need to look at the source code for the details, but here is
a glimpse of what's there:

* **Vectors:** Using Rust vectors as Haskell vectors.
* **Iterators/List:** Using Rust iterators as Haskell lists.
* **(De)serialization:** Passing values of complicated/recursive types over
FFI.
* **"Ping pong"**: Calling back a Haskell function in a Rust function that is
called by Haskell.

## Sketch of the Workflow

1. Make sure that your Rust project builds a `cdynlib` crate (cf. the options
in `rust/Cargo.toml`).
2. Write some functions with `pub extern "C" fn`. (They will often be
`unsafe`, take pointers as arguments or return some etc.)
3. "Export" the dynamic library: Generate C headers using [cbindgen][cbindgen],
and then put the header file and `lib<something>.so` (which you can find in
`target/debug` or `target/release` in your Rust project after building it)
somewhere where the Haskell tooling can find it (cf. the options in
`haskell/stack.yaml` and `haskell/package.yaml`).
4. Tell GHC how to import the functions in the header using [c2hs][c2hs].
(You can also do this without an additional tool, but `c2hs` writes some of
the boilerplate for you.)
5. While using the binaries generated from your Haskell code, make sure that
`LD_LIBRARY_PATH` contains the shared library you produced before.

The "ping pong" example requires some more bootstrapping.

## Resources

* Haskell wiki: [Foreign Function Interface](https://wiki.haskell.org/Foreign_Function_Interface)
* Documentation of `c2hs`: [Implementation of Haskell Binding Modules](https://github.com/haskell/c2hs/wiki/Implementation-of-Haskell-Binding-Modules)
* Edward Z. Yang's series: [The Haskell Preprocessor Hierarchy](http://blog.ezyang.com/2010/06/the-haskell-preprocessor-hierarchy/)
* Jake Goulding's series: [The Rust FFI Omnibus](https://wiki.haskell.org/Foreign_Function_Interface)
* Michael Bryan's posts: [Tag: FFI](https://adventures.michaelfbryan.com/tags/ffi/)


[cbindgen]: https://docs.rs/cbindgen/
[c2hs]: https://github.com/haskell/c2hs
