default: test

all: test bench docs

build: build-haskell build-rust
test: test-rust test-haskell
bench: bench-haskell
docs: docs-rust docs-haskell
clean: clean-rust clean-libraries clean-haskell

# In order to be able to bootstrap the examples, we first need to build the
# Rust library without linking it against the Haskell library.
deploy-rust-library:
	mkdir -p lib && \
	  cd rust && \
	  rustup run nightly cbindgen > ../lib/hs_rs_ffi_exp_rs.h && \
	  HS_RS_FFI_EXP_NO_LINK=1 cargo build --release && \
	  cp target/release/libhs_rs_ffi_exp_rs.so ../lib
# TODO: Find a better way to force Cargo to rebuild the project.
build-rust: build-haskell
	cd rust && \
	  cargo clean && \
	  LIBRARY_PATH=../lib cargo build --release && \
	  cp target/release/libhs_rs_ffi_exp_rs.so ../lib
# If a Rust test needs the Haskell library to be linked in the future,
# we will need to change the next rule.
test-rust:
	cd rust && HS_RS_FFI_EXP_NO_LINK=1 cargo test
docs-rust:
	cd rust && cargo doc
clean-rust:
	cd rust && cargo clean

build-haskell: deploy-rust-library
	cd haskell && \
	  stack build && \
	  cp \
	    .stack-work/dist/*/*/build/hs-rs-ffi-exp-hs/libhs-rs-ffi-exp-hs.so \
	    ../lib
test-haskell: build-rust
	cd haskell && LD_LIBRARY_PATH=../lib stack test
bench-haskell: build-rust
	cd haskell && LD_LIBRARY_PATH=../lib stack bench
docs-haskell: deploy-rust-library
	cd haskell && stack haddock
clean-haskell:
	cd haskell && stack clean

clean-libraries:
	rm -rf lib

.PHONY: clean clean-rust clean-haskell clean-library 
