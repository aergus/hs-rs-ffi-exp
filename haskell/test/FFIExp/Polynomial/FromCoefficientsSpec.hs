module FFIExp.Polynomial.FromCoefficientsSpec where

import FFIExp.Polynomial

import Test.Hspec


spec :: Spec
spec =
  describe "constructing a polynomial from a list of coefficients"
    . it "refuses to construct a polynomial with trivial leading coefficient"
    . flip shouldThrow (errorCall "Leading coefficient can't be 0.")
    . print
    $ (fromCoefficients [0, 1, 2, 4, 2, 1, 0] :: SPolynomial Int)
