module FFIExp.Polynomial.MultiplicationSpec where

import FFIExp.Polynomial
import FFIExp.Polynomial.FFI

import Control.Arrow
import Control.Monad
import Foreign.C.Types
import System.Random
import Test.Hspec


examples :: [((SPolynomial CInt, SPolynomial CInt), SPolynomial CInt)]
examples =
  ((fromCoefficients *** fromCoefficients) *** fromCoefficients) <$>
    [ (([25, -81, -127], []), [])
    , (([], [25, -81, -127]), [])
    , (([3, -2], [-2, 5]), [-6, 19, -10])
    , (([3, 8, -7, 4, 4, -1], [-2]), [-6, -16, 14, -8, -8, 2])
    , (([0, -5, 3], [1, 2, -2, 1]), [0, -5, -7, 16, -11, 3])
    ]

spec :: Spec
spec =
  describe "multiplication" $
    do it "is correct for some hand-written examples"
         . mapM_ (\ ((p, q), r) -> multiply p q `shouldBe` r)
         $ examples
       it "native and FFI code yield the same result for some random examples"
         . replicateM_ 100
         $ do [p, q] <- replicateM 2 $ randomPolynomial =<< randomRIO (1, 127)
              multiplyCIntP p q `shouldReturn` multiply p q
