module FFIExp.ExpressionSpec where

import FFIExp.Expression
import FFIExp.Expression.FFI
import Foreign.Storable

import Control.Monad
import Foreign.C.Types
import System.Random
import Test.Hspec


samples :: Int
samples = 100

sample :: Int -> IO [Expression CInt]
sample n = replicateM n $ randomExpressionR (-512, 511) =<< randomRIO (1, 1024)

additionExpectation ::
  (Eq a, Show a, Num a, Storable a) => Expression a -> Expectation
additionExpectation e =
  do let expectation = sum . flatten $ e
     evalWith (+) e `shouldBe` expectation
     evalWith' (+) e `shouldBe` expectation

serializationExpectation ::
  (Eq a, Show a, Storable a) => Expression a -> Expectation
serializationExpectation e = (decode =<< encode e) `shouldReturn` e

ffiExpectation :: Expression CInt -> Expectation
ffiExpectation e =
  do let expectation = evalWith (+) e
     addI32ThroughExpression e `shouldReturn` expectation
     addI32ThroughInstructions e `shouldReturn` expectation
     addI32Flat e `shouldReturn` expectation

spec :: Spec
spec =
  describe "expressions" $
    do it "are added correctly for some random examples"
         $ mapM_ additionExpectation =<< sample samples
       it ( "can be correctly serialized and then deserialized "
              ++ "for some random examples"
          )
         $ mapM_ serializationExpectation =<< sample samples
       it "add to the same result when evaluated natively and over FFI"
         $ mapM_ ffiExpectation =<< sample samples
