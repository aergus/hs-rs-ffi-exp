module FFIExp.RangeSpec where

import FFIExp.Range.FFI

import Foreign.C.Types
import Test.Hspec


ranges :: [(CInt, CInt)]
ranges = [(i, j) | i <- [0 .. 149], j <- [0 .. 149]]

testRange :: CInt -> CInt -> IO ()
testRange i j =
  do makeRangeCInt i j `shouldReturn` [i .. j]
     makeRangeCIntLazy i j `shouldReturn` [i .. j]
     makeDynRangeCInt i j `shouldReturn` [i .. j]
     makeDynRangeCIntLazy i j `shouldReturn` [i .. j]


spec :: Spec
spec =
  describe "range"
    . it "native and FFI code yield the same result for pre-generated examples"
    . mapM_ (uncurry testRange)
    $ ranges
