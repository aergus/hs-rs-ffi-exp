module FFIExp.LCGSpec where

import FFIExp.LCG
import FFIExp.LCG.FFI

import Control.Monad
import System.Random
import Test.Hspec


spec :: Spec
spec =
  describe "LCG"
    . it "native and FFI code yield the same result for some random examples"
    . replicateM_ 100
    $ do [mo, mu, inc, ini] <- replicateM 4 $ randomRIO (1, 1023)
         let toTake        = 100
             native        = take toTake $ makeLCG mo mu inc ini
             makeForeign f = take toTake <$> f mo mu inc ini
         makeForeign makeLCGCInt `shouldReturn` native
         makeForeign makeDynLCGCInt `shouldReturn` native
