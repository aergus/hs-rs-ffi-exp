{-# LANGUAGE FlexibleContexts #-}

import FFIExp.Polynomial
import FFIExp.Polynomial.FFI

import Control.DeepSeq
import Criterion.Main
import Foreign.C.Types
import System.Random


import qualified Data.Vector.Generic as V


type Mult a = a -> a -> a

levels :: Int
levels = 4

benchmarksFor::
     (Num n, Random n, V.Vector v n, NFData (v n))
  => ((Polynomial v n, Polynomial v n) -> Benchmarkable)
  -> [Benchmark]
benchmarksFor f =
  [ let deg = 64 * n
     in env
          ((,) <$> randomPolynomial deg <*> randomPolynomial deg)
          (bench ("degree " ++ show deg) . f)
  | n <- [0 .. (levels - 1)]
  ]

fromIO :: NFData c => (a -> b -> IO c) -> (a, b) -> Benchmarkable
fromIO = nfAppIO . uncurry

fromPure :: NFData c => (a -> b -> c) -> (a, b) -> Benchmarkable
fromPure = nf . uncurry

main :: IO ()
main =
  defaultMain
    [ bgroup "multiplication"
        [ bgroup "foreign" . benchmarksFor . fromIO $ multiplyCIntP
        , bgroup "storable" . benchmarksFor . fromPure $
            (multiply :: Mult (SPolynomial CInt))
        , bgroup "unboxed" . benchmarksFor . fromPure $
            (multiply :: Mult (UPolynomial Int))
        , bgroup "boxed" . benchmarksFor . fromPure $
            (multiply :: Mult (BPolynomial Int))
        ]
    ]
