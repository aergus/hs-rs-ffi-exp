
import FFIExp.Expression
import FFIExp.Expression.FFI

import Criterion.Main
import Foreign.C.Types


benchmarksFor :: (Expression CInt -> Benchmarkable) -> [Benchmark]
benchmarksFor f =
  [ env (randomExpressionR (-512, 511) len) (bench ("length " ++ show len) . f)
  | len <- [128, 1024, 8192]
  ]

main :: IO ()
main =
  defaultMain
    [ bgroup "expression"
        [ bgroup "lazy" . benchmarksFor . nf $ evalWith (+)
        , bgroup "strict" . benchmarksFor . nf $ evalWith' (+)
        , bgroup "ffi (expression)" . benchmarksFor . nfAppIO $
            addI32ThroughExpression
        , bgroup "ffi (instructions)" . benchmarksFor . nfAppIO $
            addI32ThroughInstructions
        , bgroup "ffi (flat)" . benchmarksFor . nfAppIO $
            addI32Flat
        ]
    ]
