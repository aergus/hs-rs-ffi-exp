{-# LANGUAGE FlexibleContexts #-}

import FFIExp.LCG
import FFIExp.LCG.FFI

import Control.DeepSeq
import Control.Monad
import Criterion.Main
import Foreign.C.Types
import System.Random


iterations :: Int
iterations = 100000

benchLCG::
     String
  -> ((CInt, CInt, CInt, CInt) -> Benchmarkable)
  -> Benchmark
benchLCG tag f =
  env randomParameters (bench tag . f)
  where
    randomParameters =
      do [mo, mu, inc, ini] <- replicateM 4 $ randomRIO (1, 1023)
         return (mo, mu, inc, ini)

uncurry4 :: (a -> b -> c -> d -> r) -> (a, b, c, d) -> r
uncurry4 f (x, y, z, t) = f x y z t

fromIO ::
     NFData r
  => (a -> b -> c -> d -> IO [r])
  -> (a, b, c, d)
  -> Benchmarkable
fromIO = nfAppIO . (fmap (take iterations) .) . uncurry4

fromPure ::
     NFData r
  => (a -> b -> c -> d -> [r])
  -> (a, b, c, d)
  -> Benchmarkable
fromPure = nf . (take iterations .) . uncurry4

main :: IO ()
main =
  defaultMain
    [ bgroup "lcg"
        [ benchLCG "native" . fromPure $ makeLCG
        , benchLCG "ffi-dyn" . fromIO $ makeDynLCGCInt
        , benchLCG "ffi" . fromIO $ makeLCGCInt
        ]
    ]
