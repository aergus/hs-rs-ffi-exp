{-# LANGUAGE MultiParamTypeClasses #-}

module FFIExp.Range.FFI where

import FFIExp.ForeignList

import Foreign.C.Types
import Foreign.Ptr

#include<hs_rs_ffi_exp_rs.h>

-- | Returns a list @[start..end]@, backed by a @DynIter_i32@.
--
-- It adapts the input to the Rust's range constructor in order to reproduce
-- Haskell's behavior of including the end of the range.
makeDynRangeCInt ::
     CInt -- ^ start
  -> CInt -- ^ end
  -> IO [CInt]
makeDynRangeCInt start end = toList =<< newDynRangeCIntFL start (end + 1)

-- | Lazy version of 'makeDynRangeCInt'.
makeDynRangeCIntLazy ::
     CInt -- ^ start
  -> CInt -- ^ end
  -> IO [CInt]
makeDynRangeCIntLazy start end =
  toListLazy =<< newDynRangeCIntFL start (end + 1)

{#pointer *RangeI32 as RangeCInt
  foreign finalizer range_i32_destruct newtype #}

{#fun unsafe range_i32_get_next as ^ {`RangeCInt', id `Ptr CInt'} -> `Bool' #}

instance ForeignList RangeCInt CInt where
  getNextFL = rangeI32GetNext

{#fun range_i32_new as newRangeCInt {`CInt', `CInt'} -> `RangeCInt' #}

-- | Returns a list @[start..end]@, backed by a @RangeI32@.
--
-- It adapts the input to the Rust's range constructor in order to reproduce
-- Haskell's behavior of including the end of the range.
makeRangeCInt ::
     CInt -- ^ start
  -> CInt -- ^ end
  -> IO [CInt]
makeRangeCInt start end = toList =<< newRangeCInt start (end + 1)

-- | Lazy version of 'makeRangeCInt'.
makeRangeCIntLazy ::
     CInt -- ^ start
  -> CInt -- ^ end
  -> IO [CInt]
makeRangeCIntLazy start end = toListLazy =<< newRangeCInt start (end + 1)
