{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module FFIExp.ForeignList where

import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Storable
import Foreign.Ptr
import System.IO.Unsafe

#include<hs_rs_ffi_exp_rs.h>


-- | A type class for foreign lists holding values of a specific type.
--
-- 'l' is usually some kind of foreign pointer, so initialization and
-- destruction is handled elsewhere.
--
-- 'getNextFL' is usually intended to be called sequentially in order to
-- receive all elements in the correct order.
-- As passing around a value o type 'l' risks 'getNextFL' being called
-- in unintended places, it is advisable to pass such a value immediately
-- to 'toList' or 'toListLazy' without exposing it to anyone else.
class Storable a => ForeignList l a | l -> a where
    getNextFL :: l -> Ptr a -> IO Bool

-- | Extracts values from 'getNextFL' until it returns 'False'.
--
-- It will "block" until all the elements are extracted, so it is not
-- suitable for infinite lists/iterators.
-- For that case, see 'toListLazy'.
toList :: ForeignList l a => l -> IO [a]
toList l =
  alloca $
    \ a_ptr ->
      do got <- getNextFL l a_ptr
         if got
           then (:) <$> peek a_ptr <*> toList l
           else return []

-- | Lazily extracts values from 'getNextFL' until it returns 'False'.
-- Hence, contrary to 'toList', this should work with infinite lists.
--
-- In order to defer rest of the computation when a value is extracted,
-- it calls 'unsafeInterleaveIO', so here be dragons.
toListLazy :: ForeignList l a => l -> IO [a]
toListLazy l =
  alloca $
    \ a_ptr ->
      do got <- getNextFL l a_ptr
         if got
           then (:) <$> peek a_ptr <*> unsafeInterleaveIO (toListLazy l)
           else return []

{#pointer *DynIter_i32 as DynIterCInt
  foreign finalizer dyn_iter_i32_destruct newtype #}

{#fun unsafe dyn_iter_i32_get_next as ^
  { `DynIterCInt'
  , id `Ptr CInt'
  } -> `Bool' #}

instance ForeignList DynIterCInt CInt where
  getNextFL = dynIterI32GetNext

-- We have to define/import some specialized functions already here because
-- otherwise @c2hs@ doesn't seem to recognize that the return type of the
-- intermediate function used to define @newDynCIntLCG@ should be a pointer
-- to 'DynIterI32' (and not a pointer to '()').

{#fun dyn_lcg_i32_new as newDynLCGCIntFL
  { `CInt'
  , `CInt'
  , `CInt'
  , `CInt'
  } -> `DynIterCInt' #}

{#fun dyn_range_i32_new as newDynRangeCIntFL
  { `CInt'
  , `CInt'
  } -> `DynIterCInt' #}
