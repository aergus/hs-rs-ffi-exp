module FFIExp.Expression.FFI where

import FFIExp.Expression

import Data.ByteString.Unsafe
import Data.Word
import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL

#include<hs_rs_ffi_exp_rs.h>

-- | Reads an expression from a strict 'BS.ByteString'.
-- (It's essentially a wrapper around 'peekExpression'.)
--
-- It isn't really relevant for FFI as we only pass a serialization over
-- FFI and don't need to deserialize anything on this side, but it useful
-- for testing.
--
-- The expected layout is as follows:
-- After each 'literalFlag', a literal is read.
-- After anything else (but normally we'd expect an 'operationFlag'), we
-- expect two serialized expressions which are read using recursive calls.
--
-- (De)serialization could probably be made more efficient and more elegant,
-- but this simple approach was chosen to minimize the dependencies and time
-- spent on figuring out a (de)serialization scheme.
decode :: Storable a => BS.ByteString -> IO (Expression a)
decode bs = unsafeUseAsCStringLen bs (peekExpression . castPtr . fst)

-- | Reads an expression from memory.
-- (It's essentially a wrapper around 'peekExpression''.)
peekExpression :: Storable a => Ptr Word8 -> IO (Expression a)
peekExpression = fmap fst . peekExpression'

-- | The function that underlies 'peekExpression'.
--
-- In addition to the expression that was read, it returns the number of bytes
-- it has read, so that offsets can be correctly set in subsequent calls to it.
peekExpression' :: Storable a => Ptr Word8 -> IO (Expression a, Int)
peekExpression' p =
  do flag <- peek p
     if flag == literalFlag
       then
         do let aP = castPtr $ p `plusPtr` 1
            x <- peek aP
            return (Literal x, 1 + sizeOf x)
       else
         do let newP = p `plusPtr` 1
            (left, leftLen) <- peekExpression' newP
            (right, rightLen) <- peekExpression' (newP `plusPtr` leftLen)
            return (Operation left right, 1 + leftLen + rightLen)

-- | Encodes an expression to a strict 'BS.ByteString' using the layout
-- described in 'peekExpression'.
-- (It just "strictifies" the result of 'encodeLazy'.)
--
-- Using bytestrings is essentially the only (de)serialization abstraction used
-- in this module:
-- The lazy 'BSL.ByteString' 'encodeLazy' produces is in some sense a recipe
-- for the strict 'BS.ByteString' this function produces, whose representation
-- contains a pointer to a memory region containing the serialization which we
-- can pass over FFI.
encode :: Storable a => Expression a -> IO BS.ByteString
encode = fmap BSL.toStrict . encodeLazy

-- | Encodes an expression to a lazy 'BSL.ByteString' using the layout
-- described in 'peekExpression'.
encodeLazy :: Storable a => Expression a -> IO BSL.ByteString
encodeLazy (Literal x) =
  (BSL.singleton literalFlag <>) . BSL.fromStrict <$> encodeLiteral x
encodeLazy (Operation l r) =
  do lBS <- encodeLazy l
     rBS <- encodeLazy r
     return $ BSL.singleton operationFlag <> lBS <> rBS

-- | Writes a value into memory and "packs" that into a strict
-- 'BS.ByteString'.
--
-- It sets a finalizer for the pointer it produces, so the pointer doesn't need
-- to be (and should not be) manually @free@d.
encodeLiteral :: Storable a => a -> IO BS.ByteString
encodeLiteral x =
  do p <- malloc
     poke p x
     unsafePackMallocCStringLen (castPtr p, sizeOf x)

-- | The byte that marks a 'Literal' in an 'Expression' (@0@).
literalFlag :: Word8
literalFlag = 0

-- | The byte that marks an 'Operation' in an 'Expression' (@1@).
operationFlag :: Word8
operationFlag = 1

-- | Passes a pointer to the serialization of the given 'Expression' to the
-- given "'IO' action".
--
-- It passes the result of 'encode' to the action using
-- 'unsafeUseAsCStringLen', which means that the action shouldn't change the
-- contents of the memory region its argument is pointing to.
withExpression :: Storable a => Expression a -> (Ptr CUChar -> IO b) -> IO b
withExpression expression action =
  do encoded <- encode expression
     unsafeUseAsCStringLen encoded (action . castPtr . fst)

{#fun unsafe add_i32_through_expression as ^
  {withExpression* `Expression CInt'} -> `CInt' #}
{#fun unsafe add_i32_through_instructions as ^
  {withExpression* `Expression CInt'} -> `CInt' #}
{#fun unsafe add_i32_flat as ^
  {withExpression* `Expression CInt'} -> `CInt' #}
