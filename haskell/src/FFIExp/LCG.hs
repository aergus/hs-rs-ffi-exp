module FFIExp.LCG where


-- | Creates an infinite list of values produced by a "linear congruential
-- generator".
--
-- The list starts with the inital value and each element @x@ is followed by
-- @(multiplier * x + increment) `rem` modulus@.
makeLCG ::
     (Num i, Integral i)
  => i -- ^ modulus
  -> i -- ^ multiplier
  -> i -- ^ increment
  -> i -- ^ initial value
  -> [i]
makeLCG modulus multiplier increment initial =
  let f x = (multiplier * x + increment) `rem` modulus
  in iterate f initial
