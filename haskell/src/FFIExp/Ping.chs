module FFIExp.Ping where

import Foreign.C.Types

#include<hs_rs_ffi_exp_rs.h>

-- | Calls the @ping_rust@ function with the given argument.
{#fun ping_rust as ping {`CULong'} -> `()'#}

foreign export ccall ping :: CULong -> IO ()
