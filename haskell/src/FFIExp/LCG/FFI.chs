{-# LANGUAGE MultiParamTypeClasses #-}

module FFIExp.LCG.FFI where

import FFIExp.ForeignList

import Foreign.C.Types
import Foreign.Ptr

#include<hs_rs_ffi_exp_rs.h>


-- | Creates an LCG that uses @DynIter_i32@ under the hood.
--
-- Given the same input, it should produce the same values as 'makeLCG'.
makeDynLCGCInt ::
     CInt -- ^ modulus
  -> CInt -- ^ multiplier
  -> CInt -- ^ increment
  -> CInt -- ^ initial value
  -> IO [CInt]
makeDynLCGCInt modulus multiplier increment initial =
  toListLazy =<< newDynLCGCIntFL modulus multiplier increment initial

{#pointer *LCG_i32 as LCGCInt foreign finalizer lcg_i32_destruct newtype #}

-- | 'getNextFL' for 'LCGCInt'.
--
-- In an ideal world, we'd just use a @call@ hook with @lcg_i32_get_next@ when
-- we define the 'ForeignList' instance, but @c2hs@ seems to think that
-- @lcg_i32_get_next@ returns a @char@.
{#fun unsafe lcg_i32_get_next as ^ {`LCGCInt', id `Ptr CInt'} -> `Bool' #}

instance ForeignList LCGCInt CInt where
  getNextFL = lcgI32GetNext

{#fun lcg_i32_new as newLCGCIntFL
  { `CInt'
  , `CInt'
  , `CInt'
  , `CInt'
  } -> `LCGCInt' #}

-- | Creates an LCG that uses @LCG_i32@ under the hood.
--
-- Given the same input, it should produce the same values as 'makeLCG'.
makeLCGCInt ::
     CInt -- ^ modulus
  -> CInt -- ^ multiplier
  -> CInt -- ^ increment
  -> CInt -- ^ initial value
  -> IO [CInt]
makeLCGCInt modulus multiplier increment initial =
  toListLazy =<< newLCGCIntFL modulus multiplier increment initial
