module FFIExp.ForeignVector where

import Foreign.C.Types
import Foreign.ForeignPtr
import Foreign.Ptr
import Foreign.Storable

import qualified Data.Vector.Generic as G
import qualified Data.Vector.Storable as V
import qualified Foreign.Concurrent as C

#include<hs_rs_ffi_exp_rs.h>


-- | A rather "transparent" representation of a foreign vector which allows
-- us to read (and technically even write) elements directly (as opposed to
-- using some kind of opaque pointer).
data ForeignVector a = ForeignVector
  { pointerFV  :: Ptr a
  , lengthFV   :: CULong
  , capacityFV :: CULong
  }

-- | Stands for "has foreign vector destructor".
class HasFVDestructor a where
  -- | Destructor for a 'ForeignVector'.
  -- Usually a foreign function.
  --
  -- In particular, our examples use Rust's vectors, which are destroyed by
  -- taking their ownership through
  -- <https://doc.rust-lang.org/std/vec/struct.Vec.html#method.from_raw_parts from_raw_parts>,
  -- which requires all the three arguments occuring here.
  fvDestructor ::
       Ptr a   -- ^ the pointer of the foreign vector
    -> CULong  -- ^ the length of the foreign vector
    -> CULong  -- ^ the capacity of the foreign vector
    -> IO ()

instance HasFVDestructor CInt where
  fvDestructor = {#call destruct_ffi_vec_i32 #}

instance Storable a => Storable (ForeignVector a) where
  -- In order to achieve genericity, we cast around some pointers...
  sizeOf _ = {#sizeof FFIVec_c_void #}
  alignment _ = {#alignof FFIVec_c_void #}
  peek p =
    ForeignVector
      <$> (castPtr <$> {#get FFIVec_c_void->pointer #} p)
      <*> {#get FFIVec_c_void->length #} p
      <*> {#get FFIVec_c_void->capacity #} p
  poke p (ForeignVector vecP l c) =
    do {#set FFIVec_c_void.pointer #} p (castPtr vecP)
       {#set FFIVec_c_void.length #} p l
       {#set FFIVec_c_void.capacity #} p c

-- | Constructs a foreign vector from a "native" vector.
--
-- It calls 'V.unsafeWith', which means that the data the foreign vector
-- points to should not be modified.
toForeign :: Storable a => V.Vector a -> IO (ForeignVector a)
toForeign v =
  V.unsafeWith v $
    \ ptr ->
        let len = fromIntegral $ V.length v
         in return $
              ForeignVector {pointerFV = ptr, lengthFV = len, capacityFV = len}

-- | Constructs a "native" vector from a foreign vector without setting any
-- finalizer for the pointer in the argument.
-- (For a version that does set a finalizer, see 'fromForeign'.)
--
-- It uses 'V.unsafeFromForeignPtr0', which implies that data pointed to by
-- by the argument may not be modified through the pointer in the argument
-- after this function is called
fromForeignRaw :: Storable a => ForeignVector a -> IO (V.Vector a)
fromForeignRaw (ForeignVector p l _) =
  V.unsafeFromForeignPtr0 <$> newForeignPtr_ p <*> pure (fromIntegral l)

-- | Similar to 'fromForeignRaw', but it also sets the available foreign vector
-- destructor as the finalizer for the 'ForeignPtr' underlying the resulting
-- vector.
--
-- You should use this function when you want to convert a vector whose buffer
-- has been allocated (and should be freed) by the external library.
--
-- It uses 'V.unsafeFromForeignPtr0', which implies that data pointed to by
-- by the argument may not be modified through the pointer in the argument
-- after this function is called.
fromForeign ::
  (Storable a, HasFVDestructor a) => ForeignVector a -> IO (V.Vector a)
fromForeign (ForeignVector p l c) =
  V.unsafeFromForeignPtr0
    <$> C.newForeignPtr p (fvDestructor p l c)
    <*> pure (fromIntegral l)

-- | Similar to 'fromForeign', but instead of constructing a 'ForeignPtr' with
-- a finalizer that calls 'fvDestructor', it calls 'fvDestructor' right away
-- after creating a new 'Vector' into which the the contents of the given
-- foreign vector is copied.
cloneForeign ::
  (Storable a, HasFVDestructor a, G.Vector v a) => ForeignVector a -> IO (v a)
cloneForeign (ForeignVector p l c) =
  do v <- G.generateM (fromIntegral l) $ peekElemOff p
     fvDestructor p l c
     return v
