module FFIExp.Polynomial.FFI where

import FFIExp.ForeignVector
import FFIExp.Polynomial

import Foreign.C.Types
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable

#include<hs_rs_ffi_exp_rs.h>


{#pointer *FFIVec_i32 as FVI32Ptr -> ForeignVector CInt #}

{#fun multiply_ffi_vec_i32 as ^
  { id `Ptr CInt'
  , `CULong'
  , id `Ptr CInt'
  , `CULong'
  , alloca- `ForeignVector CInt' peek*
  } -> `()' #}

-- | Multiplies two polynomials using `multiply_ffi_vec_i32`.
--
-- Given the same inputs, it should return the same value as 'multiply'.
multiplyCIntP :: SPolynomial CInt -> SPolynomial CInt -> IO (SPolynomial CInt)
multiplyCIntP (Polynomial l) (Polynomial r) =
  do ForeignVector l_ptr l_len _ <- toForeign l
     ForeignVector r_ptr r_len _ <- toForeign r
     result <- multiplyFfiVecI32 l_ptr l_len r_ptr r_len
     Polynomial <$> fromForeign result
