{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}

module FFIExp.Polynomial where

import Control.DeepSeq
import GHC.Generics
import System.Random

import qualified Data.Vector as V
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as M
import qualified Data.Vector.Storable as S
import qualified Data.Vector.Unboxed as U


-- | A representation of polynomials as a vector of coefficients such that
-- the coefficient of @x^n@ is the @n@-th element of the list.
--
-- In our calculations we assume that the last element of the vector (i.e.
-- the leading coefficient) is non-zero.
newtype Polynomial v n = Polynomial (v n) deriving (Eq, Show, Generic, NFData)

-- | A polynomial backed by a standard "boxed" 'V.Vector'.
type BPolynomial = Polynomial V.Vector
-- | A polynomial backed by a 'S.Vector' with 'Storable' entries.
type SPolynomial = Polynomial S.Vector
-- | A polynomial backed by a 'U.Vector' with unboxed entries.
type UPolynomial = Polynomial U.Vector

-- | Given a list @l@ of coefficients, constructs a polynomial in which the
-- coefficient of @x^n@ is @l !! n@.
--
-- Fails with @error@ if the last elemnt of @l@ is @0@.
fromCoefficients :: (Eq n, Num n, G.Vector v n) => [n] -> Polynomial v n
fromCoefficients cs =
  if not (null cs) && last cs == 0
    then error "Leading coefficient can't be 0."
    else Polynomial $ G.fromList cs

-- | Multiplies two polynomials.
multiply ::
     (Num n, G.Vector v n)
  => Polynomial v n
  -> Polynomial v n
  -> Polynomial v n
multiply (Polynomial l) (Polynomial r)
  | G.null l || G.null r =
  Polynomial G.empty
  | otherwise =
  Polynomial $ G.create $
    do v <- M.replicate (G.length l + G.length r - 1) 0
       G.iforM_ l $
         \ i x ->
           G.iforM_ r $
             \ j y ->
               M.modify v ((+) $! (x * y)) (i + j)
       return v

-- | Generates a random integral polynomial of given degree with coefficcients
-- between @-127@ and @127@.
--
-- The small range is a (rather conservative) measure to prevent overflows (and
-- underflows) while multiplying polynomials.
randomPolynomial ::
  (Num n, Random n, G.Vector v n) => Int -> IO (Polynomial v n)
randomPolynomial deg =
  fmap Polynomial . G.generateM (deg + 1) $
    \ n ->
      if n == deg
        then
          do pos <- randomIO
             if pos
               then randomRIO (1, 127)
               else randomRIO (-127, -1)
        else
          randomRIO (-127, 127)
