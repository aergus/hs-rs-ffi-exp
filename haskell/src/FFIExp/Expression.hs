{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module FFIExp.Expression where

import Control.DeepSeq
import GHC.Generics
import System.Random


-- | A type for expressions built using literals and a binary operation.
data Expression a =
  Literal a | Operation (Expression a) (Expression a)
    deriving (Eq, Show, Generic, NFData)

-- | Evaluates an expression using the given function as the "expression".
evalWith :: (a -> a -> a) -> Expression a -> a
evalWith _ (Literal x)     = x
evalWith f (Operation l r) = f (evalWith f l) (evalWith f r)

-- | A stricter version of 'evalWith' that evaluates the arguments to WHNF
-- before passing it to the function.
--
-- The strictness doesn't seem to make a big difference while adding 'CInt's...
evalWith' :: (a -> a -> a) -> Expression a -> a
evalWith' _ (Literal x)     =
  x
evalWith' f (Operation l r) =
  let l' = evalWith f l
      r' = evalWith f r
  in l' `seq` r' `seq` f l' r'

-- | Conversion into a list.
--
-- It is essentially equivalent to assuming that the operation used in the
-- expression is associative and moving all the parentheses to the rightmost
-- position.
flatten :: Expression a -> [a]
flatten (Literal x)     = [x]
flatten (Operation l r) = flatten l ++ flatten r

-- | Generates a random expression with 'Literal's in the given range and of
-- given length.
--
-- It 'fail's if the length is @0@ or negative.
randomExpressionR ::
     Random a
  => (a, a) -- ^ range for the literals
  -> Int    -- ^ length of the expression (should be @>0@)
  -> IO (Expression a)
randomExpressionR range 1 =
  Literal <$> randomRIO range
randomExpressionR range n | n > 0 =
   do middle <- randomRIO (1, n - 1)
      l <- randomExpressionR range middle
      r <- randomExpressionR range (n - middle)
      return $ Operation l r
                          | otherwise =
  fail "Can't generate an expression of non-positive length."
