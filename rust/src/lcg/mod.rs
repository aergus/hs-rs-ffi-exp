pub mod ffi;

use std::ops::{Add, Mul, Rem};

/// A representation of a "linear congruential generator".
///
/// The associated [Iterator] implementation yields arbitrarily many items
/// starting with the initial value.
/// Each item `x` is followed by `(multiplier * x + increment) % modulus`.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct LCG<T> {
    modulus: T,
    multiplier: T,
    increment: T,
    initial: T,
    current: Option<T>,
}

impl<T> LCG<T> {
    pub fn new(modulus: T, multiplier: T, increment: T, initial: T) -> Self {
        LCG {
            modulus,
            multiplier,
            increment,
            initial,
            current: None,
        }
    }
}

impl<T: Copy + Add<Output = T> + Mul<Output = T> + Rem<Output = T>> Iterator
    for LCG<T>
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match self.current {
            None => self.current = Some(self.initial),
            Some(x) => {
                self.current =
                    Some((self.multiplier * x + self.increment) % self.modulus)
            }
        }
        self.current
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn take_ten() {
        let result: Vec<i32> = LCG::new(17, 5, 11, 4).take(10).collect();
        let expectation = vec![4, 14, 13, 8, 0, 11, 15, 1, 16, 6];
        assert_eq!(result, expectation)
    }
}
