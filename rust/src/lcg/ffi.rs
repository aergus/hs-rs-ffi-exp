use super::*;
use crate::ffi::iter::DynIter;
use crate::{define_destructor_with_ident, define_iterator_ffi_with_ident};

use paste::paste;
use std::iter;

/// Creates a dynamically dispatched iterator representing an LCG that is
/// wrapped in a way that allows one to pass it over an FFI.
///
/// The actual iterator is in fact produced using [iter::successors], but it
/// should have the same behaviour as the one returned by [lcg_i32_new].
#[no_mangle]
pub extern "C" fn dyn_lcg_i32_new(
    modulus: i32,
    multiplier: i32,
    increment: i32,
    initial: i32,
) -> Box<DynIter<i32>> {
    Box::new(Box::new(iter::successors(Some(initial), move |&x| {
        Some((multiplier * x + increment) % modulus)
    })))
}

/// A wrapper around [LCG::new] for FFI.
#[no_mangle]
pub extern "C" fn lcg_i32_new(
    modulus: i32,
    multiplier: i32,
    increment: i32,
    initial: i32,
) -> Box<LCG<i32>> {
    Box::new(LCG::new(modulus, multiplier, increment, initial))
}

define_iterator_ffi_with_ident!(LCG<i32>, i32, lcg_i32);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn two_implementations() {
        let lhs: Vec<_> = dyn_lcg_i32_new(17, 5, 11, 4).take(100).collect();
        let rhs: Vec<_> = lcg_i32_new(17, 5, 11, 4).take(100).collect();
        assert_eq!(lhs, rhs)
    }

    #[test]
    fn lcg_i32_ffi() {
        let expectation = vec![4, 14, 13, 8, 0, 11, 15, 1, 16, 6];
        let mut result = Vec::new();
        unsafe {
            let it_raw = Box::into_raw(lcg_i32_new(17, 5, 11, 4));
            let p = Box::into_raw(Box::new(0));
            for _ in 0..expectation.len() {
                assert!(lcg_i32_get_next(Box::from_raw(it_raw), p));
                result.push(*p);
            }
            lcg_i32_destruct(Box::from_raw(it_raw));
            Box::from_raw(p);
        }
        assert_eq!(result, expectation)
    }
}
