use crate::define_destructor_with_ident;

use paste::paste;

/// Defines an FFI for a concrete iterator type.
///
/// It creates a destructor `identifier_destruct` and a function
///
/// ```
/// pub unsafe extern "C" fn identifier_get_next(
///     Box<iterator_type>,
///     *mut item_type,
/// ) -> bool
/// ```
///
/// The latter advances the iterator, writes the obtained value into the
/// given pointer if there is one, and returns `true` if an only if a value
/// has been obtained.
///
/// For this to work, references to values of that type have to be safe to
/// send over the FFI, but the compiler will complain if that's not the case.
///
/// **PS:** The compiler can actually read off the item type from the iterator
/// implementation for the iterator type, but we ask for that redundant
/// information because `cbindgen` can't -- if we don't name the item type
/// explicitly, it exports it as `Item`.
#[macro_export]
macro_rules! define_iterator_ffi_with_ident {
    ($iter_t:ty, $item_t:ty, $i:ident) => {
        paste! {
            define_destructor_with_ident!($iter_t, $i);
            #[no_mangle]
            pub unsafe extern "C" fn [<$i _get_next>](
                iterator_box: Box<$iter_t>,
                item_p: *mut $item_t,
            ) -> bool {
                let it = Box::leak(iterator_box);
                 match it.next() {
                    Some(x) => {
                        *item_p = x;
                        true
                    }
                    None => false
                }
            }
        }
    };
    ($iter_t:ty, $i:ident) => {
        define_iterator_ffi_with_ident!(
            $iter_t,
            <$iter_t as Iterator>::Item,
            $i
        );
    };
}

/// An alias which allows one to write down types of dynamically dispatched
/// iterators more concisely.
///
/// It is in particular useful for making `cbindgen` generate nicely named
/// C structs.
pub type DynIter<T> = Box<dyn Iterator<Item = T>>;

/// Defines an FFI for all dynamically dispatched iterators which produce
/// values of a given type.
///
/// It creates a destructor `dyn_iter_identifier_destruct` and a function
///
/// ```
/// pub unsafe extern "C" fn dyn_iter_identifier_get_next(
///     Box<DynIter<$t>>,
///     *mut $t,
/// ) -> bool
/// ```
///
/// The latter advances the iterator, writes the obtained value into the
/// given pointer if there is one, and returns `true` if an only if a value
/// has been obtained.
#[macro_export]
macro_rules! define_dyn_iterator_ffi_with_ident {
    ($t:ty, $i: ident) => {
        paste! {
            define_destructor_with_ident!(
                DynIter<$t>,
                [<dyn_iter_ $i:snake>]
            );
            #[no_mangle]
            pub unsafe extern "C" fn [<dyn_iter_ $i:snake _get_next>](
                boxed_iterator_box: Box<DynIter<$t>>,
                item_p: *mut $t
            ) -> bool {
                let boxed_iterator = Box::leak(boxed_iterator_box);
                let result = match boxed_iterator.next() {
                    Some(x) => {
                        *item_p = x;
                        true
                    }
                    None => {
                        false
                    }
                };
                result
            }
        }
    };
}

/// A convenience wrapper that repeatedly calls
/// [define_dyn_iterator_ffi_with_ident].
///
/// In each call, it passes the type in question as the identifier.
#[macro_export]
macro_rules! define_dyn_iterator_ffi {
    ($($t:ty),*) => {
        $(paste! {define_dyn_iterator_ffi_with_ident!($t, [<$t>]);})*
    };
}

define_dyn_iterator_ffi!(bool);
define_dyn_iterator_ffi!(i8, i16, i32, i64);

#[cfg(test)]
mod test {
    use super::*;

    use paste::paste;
    use std::iter;

    macro_rules! define_dyn_ffi_test {
        ($t:ty, $it:expr, $e:expr) => {
            paste! {
                #[test]
                fn [<dyn_iterator_ $t:snake _ffi>]() {
                    let expectation: Vec<_> = $it.collect();
                    let mut result = Vec::new();
                    let dyn_it_raw: *mut DynIter<$t> =
                        Box::into_raw(Box::new(Box::new($it)));
                    let p = Box::into_raw(Box::new($e));
                    unsafe {
                        while [<dyn_iter_ $t:snake _get_next>](
                            Box::from_raw(dyn_it_raw),
                            p
                        ) {
                            result.push(*p);
                        }
                        [<dyn_iter_ $t:snake _destruct>](
                            Box::from_raw(dyn_it_raw)
                        );
                        Box::from_raw(p);
                    }
                    assert_eq!(result, expectation)
                }
            }
        };
    }

    define_dyn_ffi_test!(i32, (0..10).into_iter(), 0);
    define_dyn_ffi_test!(bool, iter::repeat(true).take(42), false);
}
