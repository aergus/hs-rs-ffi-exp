use std::convert::From;
use std::ffi::c_void;
use std::slice;

/// A representation of a [Vec] suitable for passing through the FFI.
///
/// Hopefully this is "faithful" representation of Rust vectors in the sense
/// that applying [FFIVec::into_vec] after [FFIVec::from] yields an equivalent
/// vector.
#[repr(C)]
pub struct FFIVec<T> {
    pointer: *const T,
    length: usize,
    capacity: usize,
}

/// A trick to export a struct.
///
/// Its existence makes `cbindgen` generate a C struct `FFIVec_c_void`
/// containing a void pointer which can be used to figure out the
/// representation of all the other specializations of [FFIVec].
#[no_mangle]
pub extern "C" fn ffi_vec_void_len(v: FFIVec<c_void>) -> usize {
    v.length
}

impl<T> FFIVec<T> {
    pub fn get_pointer(&self) -> *const T {
        self.pointer
    }
    pub fn get_length(&self) -> usize {
        self.length
    }
    pub fn get_capacity(&self) -> usize {
        self.capacity
    }
    /// Constructs an [FFIVec] with the given data (without checking anything).
    ///
    /// It is in particular used for tests.
    pub fn new(pointer: *const T, length: usize, capacity: usize) -> Self {
        FFIVec {
            pointer,
            length,
            capacity,
        }
    }
    /// Constructs a [Vec] using [Vec::from_raw_parts].
    ///
    /// # Safety
    ///
    /// The safety the section of the documentation of [Vec::from_raw_parts]
    /// applies.
    /// In particular, may blow up if contents of `self` have been modified
    /// since being handed over from the Rust side.
    pub unsafe fn into_vec(self) -> Vec<T> {
        Vec::from_raw_parts(self.pointer as *mut T, self.length, self.capacity)
    }
    /// Constructs a slice using [slice::from_raw_parts].
    ///
    /// # Safety
    ///
    /// The safety the section of the documentation of [slice::from_raw_parts]
    /// applies.
    /// In particular, may blow up if contents of `self` have been modified
    /// since being handed over from the Rust side.
    pub unsafe fn as_slice(&self) -> &[T] {
        slice::from_raw_parts(self.pointer, self.length)
    }
}

impl<T> From<Vec<T>> for FFIVec<T> {
    fn from(v: Vec<T>) -> Self {
        let length = v.len();
        let capacity = v.capacity();
        let pointer = v.leak() as *mut [T] as *const T;
        FFIVec {
            pointer,
            length,
            capacity,
        }
    }
}

// FIXME: Looks like one needs to explicitly import `paste::paste` to be able
// to use this.
/// Defines a destructor for [FFIVec]s holding a specific type.
///
/// The generated destructor has the following signature:
/// ```
/// pub unsafe extern "C" fn destruct_ffi_vec_type_name(
///     *mut type_name,
///     usize,
///     usize,
/// )
/// ```
#[macro_export]
macro_rules! define_destruct_ffi_vec {
    ($t:ty) => {
        paste! {
            #[no_mangle]
            pub unsafe extern "C" fn [<destruct_ffi_vec_ $t>](
                ptr: *mut $t,
                len: usize,
                cap: usize,
            ) -> () {
                Vec::from_raw_parts(ptr, len, cap);
            }
        }
    };
}

#[cfg(test)]
mod test {
    use super::*;

    use std::fmt::Debug;

    // Couldn't find a way to "capture" a double free, but as of its writing,
    // the following function crashes as intended.
    //
    // #[test]
    // fn double_free() {
    //     let ffi_vec = FFIVec::from(vec!['b', 'o', 'o']);
    //     let bad_ffi_vec = FFIVec {
    //         pointer: ffi_vec.pointer,
    //         length: ffi_vec.length,
    //         capacity: ffi_vec.capacity,
    //     };
    //     unsafe {
    //         ffi_vec.into_vec();
    //     }
    //     unsafe { assert_eq!(bad_ffi_vec.into_vec()[0], 'b') }
    // }

    #[test]
    fn survives_as_slice() {
        let vec = vec![0i8, -1, -2];
        let ffi_vec = FFIVec::from(vec.clone());
        unsafe {
            let sum: i8 = ffi_vec.as_slice().iter().map(|x| *x).sum();
            assert_eq!(sum, -3);
        }
        unsafe {
            assert_eq!(ffi_vec.into_vec(), vec);
        }
    }

    #[test]
    fn to_ffi_and_back() {
        unsafe fn test_vec<T: Clone + Debug + PartialEq>(v: Vec<T>) {
            assert_eq!(v, FFIVec::from(v.clone()).into_vec())
        }
        unsafe {
            test_vec(vec![0, 1, 2, 3]);
            test_vec(vec![0usize, 1, 2, 3]);
            test_vec(vec![0i128, 1, 2, 3]);
            test_vec(vec![0.0, 2.71828, 3.14159]);
            test_vec(vec!['h', 'e', 'l', 'l', 'o']);
        }
    }
}
