pub mod iter;
pub mod ping;
pub mod vec;

/// Defines a destructor for a given type.
///
/// The generated destructor is of the form
/// ```
/// pub unsafe extern "C" fn identifier_destruct(_: Box<type_name>) {}
/// ```
///
/// Even though the function looks like it does nothing, taking the
/// ownership of the passed value means that it will be dropped when
/// the function exits.
///
/// In the exported C interface, the parameter of the function will be
/// a pointer.
#[macro_export]
macro_rules! define_destructor_with_ident {
    ($t: ty, $i: ident) => {
        paste! {
            #[no_mangle]
            pub unsafe extern "C" fn [<$i _destruct>](_: Box<$t>) {}
        }
    };
}
