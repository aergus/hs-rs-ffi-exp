extern "C" {
    fn ping(_: usize);
}

/// Prints the argument, and if the argument `n` is greater than `0`, calls
/// back Haskell's `ping` function with `n-1` as the argument.
///
/// Looks like it doesn't need to initialize/manage a Haskell runtime when it
/// is called from Haskell...
#[no_mangle]
pub extern "C" fn ping_rust(n: usize) {
    println!("{}", n);
    if n > 0 {
        unsafe { ping(n - 1) }
    }
}
