use super::*;

use std::mem;

/// The byte that marks a literal in an [Expression] (`0`).
pub static LITERAL_FLAG: u8 = 0;
/// The byte that marks an operation in an [Expression] (`1`).
pub static OPERATION_FLAG: u8 = 1;

impl Expression<i32> {
    /// Serializes an [Expression]. Used for testing.
    pub fn serialize(self) -> Vec<u8> {
        let instruction_vec = self.compile().0;
        let num_lits = (instruction_vec.len() + 1) / 2;
        let num_ops = instruction_vec.len() - num_lits;
        let mut result = Vec::with_capacity(
            num_ops + (1 + mem::size_of::<i32>()) * num_lits,
        );
        for instruction in instruction_vec {
            match instruction {
                None => result.push(OPERATION_FLAG),
                Some(x) => {
                    result.push(LITERAL_FLAG);
                    result.extend_from_slice(&x.to_ne_bytes());
                }
            }
        }
        result
    }
}

impl<T: Clone> Instructions<T> {
    /// Parses the serialization of an [Expression] as [Instructions].
    ///
    /// This approach allows us to avoid recursive calls by adding
    /// an operation instruction (i.e. [None]) when a [OPERATION_FLAG] is
    /// encountered instead of recursively parsing the arguments.
    ///
    /// # Safety
    ///
    /// The argument pointer should point to the beginning of a memory region
    /// that cointains a valid serialization of an [Expression].
    pub unsafe fn deserialize(mut p: *const u8) -> Self {
        let mut instruction_vec = Vec::new();
        let mut literals_expected: usize = 1;
        while literals_expected > 0 {
            let flag = *p;
            if flag == LITERAL_FLAG {
                p = p.add(1);
                let lit = (p as *const T).as_ref().unwrap().clone();
                instruction_vec.push(Some(lit));
                p = p.add(mem::size_of::<T>());
                literals_expected -= 1;
            } else {
                instruction_vec.push(None);
                p = p.add(1);
                literals_expected += 1;
            }
        }
        Instructions(instruction_vec)
    }
}

impl<T: Clone> Expression<T> {
    /// Deserializes an [Expression].
    ///
    /// Uses [Expression::deserialize_with] internally.
    ///
    /// # Safety
    ///
    /// The argument pointer should point to the beginning of a memory region
    /// that cointains a valid serialization of an [Expression].
    pub unsafe fn deserialize(p: *const u8) -> Self {
        Self::deserialize_with(p, Expression::Literal, |l, r| {
            Expression::Operation(Box::new(l), Box::new(r))
        })
    }
    /// Deserializes an [Expression] to [Instructions] and executes those
    /// instructions using the given functions.
    ///
    /// It basically calls [Instructions::interpret_with] after
    /// [Instructions::deserialize].
    ///
    /// # Safety
    ///
    /// The argument pointer should point to the beginning of a memory region
    /// that cointains a valid serialization of an [Expression].
    pub unsafe fn deserialize_with<S, F: FnMut(T) -> S, G: FnMut(S, S) -> S>(
        p: *const u8,
        f: F,
        g: G,
    ) -> S {
        Instructions::deserialize(p).interpret_with(f, g)
    }
}

/// Deserializes an [Expression] of [i32]s and evaluates the result using
/// addition as the operation.
///
/// It uses [Expression::deserialize] and [Expression::eval_with] internally,
/// so actually "traverses" the expression twice, which is unnecessary.
/// See [add_i32_through_instructions] for a more efficient implementation.
///
/// # Safety
///
/// The argument pointer should point to the beginning of a memory region
/// that cointains a valid serialization of an [Expression].
#[no_mangle]
pub unsafe extern "C" fn add_i32_through_expression(p: *const u8) -> i32 {
    Expression::deserialize(p).eval_with(|x| x, |x, y| x + y)
}

/// Deserializes an [Expression] of [i32]s to [Instructions] and executes
/// those using addition as the operation.
///
/// It uses [Expression::deserialize_with] internally.
///
/// # Safety
///
/// The argument pointer should point to the beginning of a memory region
/// that cointains a valid serialization of an [Expression].
#[no_mangle]
pub unsafe extern "C" fn add_i32_through_instructions(p: *const u8) -> i32 {
    Expression::deserialize_with(p, |x| x, |x, y| x + y)
}

/// Evaluates an [Expression] of [i32]s by iterating through the serialization
/// and adding up all the literals encountered.
///
/// It demonstrates that in this concrete example, we don't even have to
/// juggle around [Expression]s.
/// (However, they have their own merit as not all binary operations are
/// associative...)
///
/// # Safety
///
/// The argument pointer should point to the beginning of a memory region
/// that cointains a valid serialization of an [Expression].
#[no_mangle]
pub unsafe extern "C" fn add_i32_flat(mut p: *const u8) -> i32 {
    let mut result = 0;
    let mut literals_expected: usize = 1;
    while literals_expected > 0 {
        let flag = *p;
        if flag == LITERAL_FLAG {
            p = p.add(1);
            result += *(p as *const i32);
            p = p.add(mem::size_of::<i32>());
            literals_expected -= 1;
        } else {
            p = p.add(1);
            literals_expected += 1;
        }
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn deserialization() {
        let expressions: Vec<Expression<i32>> = vec![
            Expression::Literal(3),
            Expression::Literal(-1).op(Expression::Literal(2)),
            Expression::Literal(0)
                .op(Expression::Literal(63))
                .op(Expression::Literal(-77)),
            Expression::Literal(-1)
                .op(Expression::Literal(12).op(Expression::Literal(0)))
                .op(Expression::Literal(42)),
        ];
        for e in expressions {
            unsafe {
                assert_eq!(
                    Expression::deserialize(e.clone().serialize().as_ptr()),
                    e
                );
            }
        }
    }
}
