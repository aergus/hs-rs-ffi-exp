/// Exported functions and (de)serialization code for [Expression]s.
///
/// The (de)serialization layout is as follows:
/// After each [ffi::LITERAL_FLAG], a literal is read.
/// After anything else (but normally we'd expect an [ffi::OPERATION_FLAG]),
/// we expect two serialized expressions.
pub mod ffi;

/// A type for expressions built using literals and a binary operation.
#[derive(Clone, Debug, PartialEq)]
pub enum Expression<T> {
    Literal(T),
    Operation(Box<Expression<T>>, Box<Expression<T>>),
}

/// A representation of an [Expression] as a "list of instructions" (to be
/// "executed in a stack machine").
///
/// Was too lazy to define a separate type for instructions, so [Some] means
/// "push a literal to the stack" and [None] means "pop two literals from the
/// stack and apply the operation to them".
#[derive(Clone, Debug, PartialEq)]
pub struct Instructions<T>(Vec<Option<T>>);

impl<T> Instructions<T> {
    /// "Executes" the given [Instructions] in a "stack stack machine",
    /// transforming the literals using the first function and applying
    /// the second function when an operation is encountered.
    pub fn interpret_with<S, F: FnMut(T) -> S, G: FnMut(S, S) -> S>(
        self,
        mut f: F,
        mut g: G,
    ) -> S {
        let stack_size = (self.0.len() + 1) / 2;
        let mut stack = self.0.into_iter().rev().fold(
            Vec::with_capacity(stack_size),
            |mut s, i| match i {
                Some(x) => {
                    s.push(f(x));
                    s
                }
                None => {
                    let l = s.pop();
                    debug_assert!(
                        l.is_some(),
                        "Stack underflow: Can't pop left argument."
                    );
                    let l = l.unwrap();
                    let r = s.pop();
                    debug_assert!(
                        r.is_some(),
                        "Stack underflow: Can't pop right argument."
                    );
                    let r = r.unwrap();
                    s.push(g(l, r));
                    s
                }
            },
        );
        debug_assert!(
            stack.len() == 1,
            "The stack doesn't contain exactly one element after evaluation.",
        );
        stack.pop().unwrap()
    }
}

impl<T> Expression<T> {
    /// Produces an [Expression::Operation] variant in which `self` is the left
    /// argument and the given other [Expression] is the right argument.
    pub fn op(self, r: Self) -> Self {
        Self::Operation(Box::new(self), Box::new(r))
    }
    /// Evaluate an [Expression], transforming the literals using the first
    /// function and applying the second function when an operation is
    /// encountered.
    ///
    /// Uses a "stack machine" internally as an exercise in avoiding recursive
    /// calls.
    pub fn eval_with<S, F: FnMut(T) -> S, G: FnMut(S, S) -> S>(
        self,
        f: F,
        g: G,
    ) -> S {
        self.compile().interpret_with(f, g)
    }
    /// Produces [Instructions] which describe how to evaluate the given
    /// [Expression].
    ///
    /// The results contains the instructions in "Polish notation", so a "stack
    /// machine" working with "reverse Polish notation" should execute them
    /// "backwards".
    pub fn compile(self) -> Instructions<T> {
        let mut stack = vec![self];
        let mut instruction_vec = Vec::new();
        while !stack.is_empty() {
            match stack.pop().unwrap() {
                Expression::Literal(x) => {
                    instruction_vec.push(Some(x));
                }
                Expression::Operation(l, r) => {
                    instruction_vec.push(None);
                    stack.push(*r);
                    stack.push(*l);
                }
            }
        }
        Instructions(instruction_vec)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parenthesis() {
        let expectation = String::from("((a(bc))d)");
        let result = Expression::Literal(String::from("a"))
            .op(Expression::Literal(String::from("b"))
                .op(Expression::Literal(String::from("c"))))
            .op(Expression::Literal(String::from("d")))
            .eval_with(|x| x, |x, y| format!("({}{})", x, y));
        assert_eq!(result, expectation)
    }
}
