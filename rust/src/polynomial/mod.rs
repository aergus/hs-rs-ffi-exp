pub mod ffi;

use num_traits::Zero;
use std::fmt::Debug;
use std::ops::{AddAssign, Mul};

/// A representation of polynomials as a vector of coefficients.
///
/// A vector representation `v` means that the coefficient of `x^n` is `v[n]`.
///
/// In our calculations, we assume that the last element of the vector (i.e.
/// the "leading coefficient" is non-zero).
#[derive(Clone, Debug, PartialEq)]
pub struct Polynomial<T>(Vec<T>);

impl<T: Debug + PartialEq + Zero> Polynomial<T> {
    /// Creates a vector from a vector of coefficients.
    /// Panics if the last element of the vector is zero.
    pub fn new(v: Vec<T>) -> Polynomial<T> {
        match v.last() {
            Some(x) if *x == Zero::zero() => {
                panic!(
                    "Leading coefficient can't be 0. (Input vector: {:?})",
                    v
                )
            }
            _ => Polynomial(v),
        }
    }
}

fn multiply_slices<T: Copy + AddAssign + Mul<Output = T> + Zero>(
    l: &[T],
    r: &[T],
) -> Vec<T> {
    if l.is_empty() || r.is_empty() {
        vec![]
    } else {
        let mut o = vec![Zero::zero(); l.len() + r.len() - 1];
        for (i, &x) in l.iter().enumerate() {
            for (j, &y) in r.iter().enumerate() {
                o[i + j] += x * y;
            }
        }
        o
    }
}

/// Multiplies two polynomials.
pub fn multiply<T: Copy + AddAssign + Mul<Output = T> + Zero>(
    Polynomial(l): &Polynomial<T>,
    Polynomial(r): &Polynomial<T>,
) -> Polynomial<T> {
    Polynomial(multiply_slices(l.as_slice(), r.as_slice()))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    #[should_panic]
    fn trivial_leading_coefficient() {
        Polynomial::new(vec![0, 1, 2, 4, 2, 1, 0]);
    }

    #[test]
    fn multiplication_with_zero() {
        let zero = Polynomial::new(vec![]);
        let p = Polynomial::new(vec![25, -81, -127]);
        assert_eq!(multiply(&p, &zero), zero);
        assert_eq!(multiply(&zero, &p), zero);
    }

    #[test]
    fn linear_times_linear() {
        let p = Polynomial::new(vec![3, -2]);
        let q = Polynomial::new(vec![-2, 5]);
        let s = Polynomial::new(vec![-6, 19, -10]);
        assert_eq!(multiply(&p, &q), s);
    }

    #[test]
    fn degree_five_times_constant() {
        let p = Polynomial::new(vec![3, 8, -7, 4, 4, -1]);
        let q = Polynomial::new(vec![-2]);
        let s = Polynomial::new(vec![-6, -16, 14, -8, -8, 2]);
        assert_eq!(multiply(&p, &q), s);
    }

    #[test]
    fn quadratic_times_cubic() {
        let p = Polynomial::new(vec![0, -5, 3]);
        let q = Polynomial::new(vec![1, 2, -2, 1]);
        let s = Polynomial::new(vec![0, -5, -7, 16, -11, 3]);
        assert_eq!(multiply(&p, &q), s);
    }
}
