use super::*;

use crate::define_destruct_ffi_vec;
use crate::ffi::vec::FFIVec;
use paste::paste;
use std::slice;

/// Defines a polynomial multiplication FFI for a concrete type.
///
/// It creates a function with the following signature:
///
/// ```
/// pub unsafe extern "C" fn multiply_ffi_vec_type_name(
///     l_ptr: *const type_name,
///     l_len: usize,
///     r_ptr: *const type_name,
///     r_len: usize,
///     p: *mut FFIVec<type_name>,
/// )
/// ```
///
/// The generated function interprets the pairs of arguments as slices and
/// writes the result of the multiplication into the given pointer.
#[macro_export]
macro_rules! define_multiply_ffi_vec {
    ($t:ty) => {
        paste! {
            #[no_mangle]
            pub unsafe extern "C" fn [<multiply_ffi_vec_ $t>](
                l_ptr: *const $t,
                l_len: usize,
                r_ptr: *const $t,
                r_len: usize,
                p: *mut FFIVec<$t>,
            ) {
                assert!(
                    !p.is_null(),
                    "call to `FFIVec` multiplication w/ a null output pointer",
                );
                *p = FFIVec::from(multiply_slices(
                    slice::from_raw_parts(l_ptr, l_len),
                    slice::from_raw_parts(r_ptr, r_len),
                ));
            }
        }
    };
}

define_destruct_ffi_vec!(i32);
define_multiply_ffi_vec!(i32);

#[cfg(test)]
mod test {
    use super::*;

    use std::ptr;

    #[test]
    #[should_panic]
    fn output_pointer_null() {
        let p = FFIVec::from(vec![]);
        let q = FFIVec::from(vec![]);
        unsafe {
            multiply_ffi_vec_i32(
                p.get_pointer(),
                p.get_length(),
                q.get_pointer(),
                q.get_length(),
                ptr::null_mut(),
            );
        }
    }

    #[test]
    fn i32_linear() {
        let p = FFIVec::from(vec![3, -2]);
        let q = FFIVec::from(vec![-2, 5]);
        let s_slice = [-6, 19, -10];
        // Once/if they stabilize, we could use a `new_uninit`-`assume_init`
        // combo here.
        let s_ptr = Box::into_raw(Box::new(FFIVec::new(ptr::null(), 0, 0)));
        unsafe {
            multiply_ffi_vec_i32(
                p.get_pointer(),
                p.get_length(),
                q.get_pointer(),
                q.get_length(),
                s_ptr,
            );
            assert_eq!((*s_ptr).as_slice(), s_slice);
        }
    }
}
