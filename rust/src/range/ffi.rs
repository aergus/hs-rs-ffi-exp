use crate::ffi::iter::DynIter;
use crate::{define_destructor_with_ident, define_iterator_ffi_with_ident};

use paste::paste;
use std::ops::Range;

/// A wrapper to give `cbindgen` a better name to export.
///
/// Even with a type alias, `cbindgen` complains about not being able to
/// resolve some generics...
pub struct RangeI32(Range<i32>);

impl Iterator for RangeI32 {
    type Item = i32;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

/// A wrapper around `start..and` for FFI (with dynamic dispatch).
#[no_mangle]
pub extern "C" fn dyn_range_i32_new(start: i32, end: i32) -> Box<DynIter<i32>> {
    Box::new(Box::new(start..end))
}

/// A wrapper around `start..end` for FFI.
#[no_mangle]
pub extern "C" fn range_i32_new(start: i32, end: i32) -> Box<RangeI32> {
    Box::new(RangeI32(start..end))
}

define_iterator_ffi_with_ident!(RangeI32, i32, range_i32);
