use std::env;

// TODO: Generate C headers here.
fn main() {
    if env::var("HS_RS_FFI_EXP_NO_LINK").is_err() {
        println!("cargo:rustc-link-lib=hs-rs-ffi-exp-hs")
    }
}
